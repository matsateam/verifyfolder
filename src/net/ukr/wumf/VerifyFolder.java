package net.ukr.wumf;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VerifyFolder implements Runnable {

	private File directory;
	private File[] checkArrBefore;
	private File[] checkArrNow;

	public VerifyFolder(File directory) {
		super();
		this.directory = directory;
	}

	public VerifyFolder() {
		super();
	}

	public void findChange() {
		for (int i = 0; i < checkArrBefore.length; i++) {
			for (int k = 0; k < checkArrNow.length; k++) {
				if (checkArrBefore[i].equals(checkArrNow[k])) {
					checkArrBefore[i] = null;
					checkArrNow[k] = null;
					break;
				}
			}
		}
	}

	public void checkDir() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");

		for (int i = 0; i < checkArrBefore.length; i++) {
			long checkChanges = System.currentTimeMillis() - checkArrBefore[i].lastModified();
			if (checkChanges < 1000) {
				System.out.println(
						sdf.format(date) + "\n" + "���������� ����� " + "\n" + checkArrBefore[i].getName() + " ���� ��������");
			}
		}

		findChange();

		for (int i = 0; i < checkArrBefore.length; i++) {
			if (checkArrBefore[i] != null) {
				System.out.println(sdf.format(date)+ "\n"  + "���� " + "\n" + checkArrBefore[i].getName() + " ��� ������");
			}
		}

		for (int i = 0; i < checkArrNow.length; i++) {
			if (checkArrNow[i] != null) {
				if (checkArrNow[i].isDirectory()) {
					System.out.println(sdf.format(date)+ "\n" + "������� " + "\n" + checkArrNow[i].getName() + " ��� ��������");
				} else {
					System.out.println(sdf.format(date)+ "\n" + "���� " + "\n" + checkArrNow[i].getName() + " ��� ��������");
				}
			}
		}
	}

	@Override
	public void run() {
		checkArrBefore = directory.listFiles();
		try {
			Thread.sleep(1000);
			checkArrNow = directory.listFiles();
			checkDir();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		run();
	}

}
